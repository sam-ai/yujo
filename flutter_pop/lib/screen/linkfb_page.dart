import 'package:flutter/material.dart';


class LinkPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purpleAccent,
        title: Text("Link Page"),
      ),
      body: new LinkPageWidget(),
    );
  }
}

class LinkPageWidget extends StatefulWidget {
  @override
  DemoState createState() => new DemoState();
}

class DemoState extends State<LinkPageWidget> {
  Map<String, bool> values = {
    'foo': true,
    'bar': false,
  };

  @override
  Widget build(BuildContext context) {
    return new ListView(
      children: values.keys.map((String key) {
        return new CheckboxListTile(
          title: new Text(key),
          value: values[key],
          onChanged: (bool value) {
            setState(() {
              values[key] = value;
            });
          },
        );
      }).toList(),
    );
  }
}
