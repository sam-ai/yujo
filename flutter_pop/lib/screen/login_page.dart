import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;
import '../utils/navigation_router.dart';
import '../utils/display/color.dart';

//import 'intro_page.dart';
//import 'IntroPage.dart';

// get the text in the TextField and start the Second Screen
//void _sendDataToSecondScreen(BuildContext context, String simpleText) {
//  String textToSend = simpleText;
//  Navigator.push(
//      context,
//      MaterialPageRoute(
//        builder: (context) => IntroPage(text: textToSend,), // SecondScreen(text: textToSend,)
//      ));
//}



class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isLoggedIn = false;
  var profileData;
  var facebookLogin = FacebookLogin();

  void onLoginStatusChanged(bool isLoggedIn, {profileData}) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
      this.profileData = profileData;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
//        appBar: AppBar(
//          backgroundColor: Colors.purpleAccent,
//          title: Text("Facebook Login"),
//          actions: <Widget>[
//            IconButton(
//              icon: Icon(
//                Icons.exit_to_app,
//                color: Colors.pink,
//              ),
////              onPressed: () => Navigator.pushNamed(context, '/intro'),
//              onPressed: () => facebookLogin.isLoggedIn
//                  .then((isLoggedIn) => isLoggedIn ? _logout() : {}),
//            ),
//          ],
//        ),
        body: Container(
          color: PrimaryColor,
          child: Center(
            child: isLoggedIn
                ? _displayUserData(profileData)
                : _displayLoginButton(),
          ),
        ),
      ),
    );
  }

  void initiateFacebookLogin() async {
    print("Init fb login process");
//    var facebookLogin = await FacebookLogin();
    facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
    var facebookLoginResult = await facebookLogin.logIn(['email', 'manage_pages']);

    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.cancelledByUser:
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.loggedIn:
        print(facebookLoginResult.accessToken.token.toString());
        var graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200),accounts&access_token=${facebookLoginResult
                .accessToken.token}');

        var profile = json.decode(graphResponse.body);
        print(profile.toString());

        onLoginStatusChanged(true, profileData: profile);
        break;
    }
  }

  _displayUserData(profileData) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: 200.0,
          width: 200.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              fit: BoxFit.fill,
              image: NetworkImage(
                profileData['picture']['data']['url'],
              ),
            ),
          ),
        ),
        SizedBox(height: 28.0),
        Text(
          "Logged in as: ${profileData['name']}",
          style: TextStyle(
            fontSize: 20.0,
          ),
        ),
      ],
    );
  }

  _displayLoginButton() {
    return RaisedButton(
      child: Text("Login with Facebook"),
      color: FacebookLoginButton,
//      onPressed: () => _sendDataToSecondScreen(context, "sambit"),
//      onPressed: () => initiateFacebookLogin(),
      onPressed: () => NavigationRouter.switchToIntro_test(context, "i am GOD"),
    );
  }

  _logout() async {
    await facebookLogin.logOut();
    onLoginStatusChanged(false);
    print("Logged out");
  }
}

class ScreenArguments {
  final String title;
  final String message;

  ScreenArguments(this.title, this.message);
}


/////////////////////////////////////////////////////////////////////

//class Photo {
//  final int id;
//  final String title;
//  final String thumbnailUrl;
//
//  Photo({this.id, this.title, this.thumbnailUrl});
//
//  factory Photo.fromJson(Map<String, dynamic> json) {
//    return Photo(
//      id: json['id'] as int,
//      title: json['title'] as String,
//      thumbnailUrl: json['thumbnailUrl'] as String,
//    );
//  }
//}
//
//// A function that converts a response body into a List<Photo>.
//List<Photo> parsePhotos(String responseBody) {
//  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
//
//  return parsed.map<Photo>((json) => Photo.fromJson(json)).toList();
//}
//
//Future<List<Photo>> fetchPhotos(http.Client client) async {
//
//  final token = 'WIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ikpv';
//
////  Map<String, String> headers = {
////    HttpHeaders.contentTypeHeader: "application/json", // or whatever
////    HttpHeaders.authorizationHeader: "Bearer $token",
////  };
//  Map<String, String> headers = {"Content-type": "application/json"};
//  final response =
//  await client.post('https://jsonplaceholder.typicode.com/photos', headers: headers);
//
//  return parsePhotos(response.body);
//}