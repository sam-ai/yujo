import 'package:flutter/material.dart';


//class SecondRoute extends StatelessWidget {
//
//  @override
//  Widget build(BuildContext context) {
//    return IntroPageWidget();
//  }
//}
//
class IntroPage extends StatelessWidget {
  final String text;

  IntroPage({Key key, @required this.text}) : super(key: key);
//  final String message;
//
//  const IntroPage({
//    Key key,
//    @required this.title,
//    @required this.message,
//    }) : super(key: key);


  @override
  Widget build(BuildContext context) {
//    print(title.toString());
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purpleAccent,
        title: Text(text),
//        title: Text(title),
      ),
      body: new IntroPageWidget(),
    );
  }
}



//class IntroPage extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    final appTitle = 'Intro Page';
//
//    return MaterialApp(
//      title: appTitle,
//      home: Scaffold(
//        appBar: AppBar(
//          title: Text(appTitle),
//        ),
//        body: new IntroPageWidget(),
//      ),
//    );
//  }
//}
//
class IntroPageWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

class _LoginData {
  String email = '';
  String password = '';
}

class _LoginPageState extends State<IntroPageWidget> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();




  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final Size screenSize = MediaQuery.of(context).size;
    return new Container(
      padding: new EdgeInsets.all(20.0),
      child: new Form(
        key: this._formKey,
        child: new ListView(
          children: <Widget>[
            new TextFormField(
                keyboardType: TextInputType.emailAddress, // Use email input type for emails.
                decoration: new InputDecoration(
                    hintText: 'you@example.com',
                    labelText: 'E-mail Address'
                )
            ),
            new DropdownButton<String>(
              hint:  Text('Please choose a Business'),
              items: <String>['Saloon', 'Clinic', 'Resto', 'Others'].map((String value) {
                return new DropdownMenuItem<String>(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
              onChanged: (value) {
                print(value.toString());
              },
            ),
            new Container(
              width: screenSize.width,
              child: new RaisedButton(
                child: new Text(
                  'Login',
                  style: new TextStyle(
                      color: Colors.white
                  ),
                ),
                onPressed: () => Navigator.pushNamed(context, '/dashboard'),
                color: Colors.purpleAccent,
              ),
              margin: new EdgeInsets.only(
                  top: 20.0
              ),
            )
          ],
        ),
      ),
    );
  }
}

//class _LoginPageState extends State<IntroPageWidget> {
//
//  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
//
//  @override
//  Widget build(BuildContext context) {
//    final Size screenSize = MediaQuery.of(context).size;
//
//    return new Scaffold(
//      appBar: new AppBar(
//        backgroundColor: Colors.purpleAccent,
//        title: new Text('Login'),
//      ),
//      body: new Container(
//          padding: new EdgeInsets.all(20.0),
//          child: new Form(
//            key: this._formKey,
//            child: new ListView(
//              children: <Widget>[
//                new TextFormField(
//                    keyboardType: TextInputType.emailAddress, // Use email input type for emails.
//                    decoration: new InputDecoration(
//                        hintText: 'you@example.com',
//                        labelText: 'E-mail Address'
//                    )
//                ),
//                new TextFormField(
//                    obscureText: true, // Use secure text for passwords.
//                    decoration: new InputDecoration(
//                        hintText: 'Password',
//                        labelText: 'Enter your password'
//                    )
//                ),
//                new Container(
//                  width: screenSize.width,
//                  child: new RaisedButton(
//                    child: new Text(
//                      'Login',
//                      style: new TextStyle(
//                          color: Colors.white
//                      ),
//                    ),
//                    onPressed: () => null,
//                    color: Colors.purpleAccent,
//                  ),
//                  margin: new EdgeInsets.only(
//                      top: 20.0
//                  ),
//                )
//              ],
//            ),
//          )
//      ),
//    );
//  }
//}
