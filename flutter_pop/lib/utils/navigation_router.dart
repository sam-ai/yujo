import 'package:flutter/material.dart';
import 'package:flutter_pop/utils/services.dart';
import '../screen/intro_page.dart';




// get the text in the TextField and start the Second Screen
void _sendDataToSecondScreen(BuildContext context, String simpleText) {
  String textToSend = simpleText;
  Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => IntroPage(text: textToSend,), // SecondScreen(text: textToSend,)
      ));
}

class NavigationRouter {
  static void switchToLogin(BuildContext context) {
    Navigator.pushNamed(context, "/login");
  }

  static void switchToIntro(BuildContext context) {
    Navigator.pushNamed(context, "/intro");
  }

  static void switchToIntro_test(BuildContext context, String sendText) {
    send_data_to_intro_screen(context, sendText);
  }

  static void switchToHome(BuildContext context) {
    Navigator.pushNamed(context, "/demo");
  }
  static void switchToProfile(BuildContext context) {
    Navigator.pushNamed(context, "/dashboard");
  }


  void send_data_to_second_screen(BuildContext context, String simpleText) {
    String textToSend = simpleText;
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => IntroPage(text: textToSend,)
        ));
  }
}