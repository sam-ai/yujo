import 'package:flutter/material.dart';
import '../screen/intro_page.dart';



// get the text in the TextField and start the Second Screen
send_data_to_intro_screen(BuildContext context, String simpleText) {
  String textToSend = simpleText;
  Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => IntroPage(text: textToSend,), // SecondScreen(text: textToSend,)
      ));
}