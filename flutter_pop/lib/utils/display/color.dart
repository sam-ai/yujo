import 'package:flutter/material.dart';


const PrimaryColor =  Color(0xFF808080);
const FacebookLoginButton = Colors.blueAccent;
const PrimaryAssentColor =  Color(0xFF808080);
const PrimaryDarkColor =  Color(0xFF808080);
const ErroColor =  Color(0xFF808080);