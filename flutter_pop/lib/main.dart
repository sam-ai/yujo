//import 'dart:convert';
//import 'dart:js';
import 'package:flutter/material.dart';
//import 'package:flutter_facebook_login/flutter_facebook_login.dart';
//import 'package:http/http.dart' as http;



//import 'pages/LoginPage.dart';
//import 'pages/IntroPage.dart';
//import 'pages/LinkfbPage.dart';
//import 'pages/DashboardPage.dart';
//import 'pages/splashPage.dart';



import 'screen/splash_page.dart';
import 'screen/login_page.dart';
import 'screen/intro_page.dart';
import 'screen/linkfb_page.dart';
import 'screen/dashboard_page.dart';



void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: true,
    title: 'Named Routes Demo',
    // Start the app with the "/" named route. In this case, the app starts
    // on the FirstScreen widget.
    initialRoute: '/',
    routes: {
      // When navigating to the "/" route, build the FirstScreen widget.
      '/': (context) => SplashScreen(),
      '/login': (context) => LoginPage(),
      '/intro': (context) => IntroPage(),
      '/demo': (context) => LinkPage(),
      '/dashboard': (context) => DashBoard(),
    },
  ));
}

